## API documentation for registration and authorization
### Table of contents

- [Login](#login)
- [Registration](#registration)
- [Find by user ID](#find-by-user-id)

### Login

#### Description
Sign up users

#### HTTP request

````
GET /api/v1/auth/login

Content-Type: application/json
Required token: no
````

#### HTTP body request params
````
    "username": Имя юзера
    "password": Пароль
````

#### Sample body request
````
200 OK
"username": admin
"password": admin
````



#### Errors

````
    No Errors
````

### Registration

#### Description
Registration users

#### HTTP request

````
POST /api/v1/auth/registration

Content-Type: application/json
Required token: no
````

#### HTTP body request params
````
    "username": Имя юзера //String
    "password": Пароль //String
````

#### Sample body request
````
    "username": admin //String
    "password": admin //String
````

#### Sample response

````
200 OK
````

#### Errors

````
    No Errors
````
### Find by user ID

#### Description
Find by user ID

#### HTTP request

````
GET /api/v1/users/{id}

Content-Type: application/json
Required token: yes
````

#### HTTP body request params
````
    "username": Имя юзера //String
    "password": Пароль //String
````

#### Sample body request
````
    "username": admin //String
    "password": admin //String
````

#### Sample response

````
200 OK
````

#### Errors

````
    No Errors
````