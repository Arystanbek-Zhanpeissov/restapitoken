package com.example.demo.dto;

import com.example.demo.entity.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserRequestDto {
    private Long id;
    private String username;

    public User toUser(){
        User user = new User();
        user.setId(id);
        user.setUsername(username);
        return user;
    }

    public static UserRequestDto fromUser(User user) {
        UserRequestDto userDto = new UserRequestDto();
        userDto.setId(user.getId());
        userDto.setUsername(user.getUsername());
        return userDto;
    }
}



