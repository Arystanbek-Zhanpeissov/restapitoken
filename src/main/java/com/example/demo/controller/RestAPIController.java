package com.example.demo.controller;

import com.example.demo.dto.AuthenticationRequestDto;
import com.example.demo.dto.UserRequestDto;
import com.example.demo.entity.User;
import com.example.demo.security.jwt.JwtProvider;
import com.example.demo.service.UserService;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.impl.DefaultClaims;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@RestController
@RequestMapping(value = "/api/v1/auth/")
@Slf4j
public class RestAPIController {

    @Value("${jwt.token.expired}")
    private long validityInMilliseconds;

    @Value("${jwt.refreshToken.expired}")
    private long refreshValidityInMilliseconds;

    private final AuthenticationManager authenticationManager;

    private final JwtProvider jwtTokenProvider;

    private final UserService userService;

    private final BCryptPasswordEncoder passwordEncoder;

    @Autowired
    public RestAPIController(AuthenticationManager authenticationManager, JwtProvider jwtTokenProvider, UserService userService, BCryptPasswordEncoder passwordEncoder) {
        this.authenticationManager = authenticationManager;
        this.jwtTokenProvider = jwtTokenProvider;
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
    }

    @PostMapping("/registration")
    public User addUser(@RequestBody User user) {
        return userService.addUser(user);
    }

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody AuthenticationRequestDto requestDto) {
        try {
            String username = requestDto.getUsername();
            log.info("Get username :{}", username);
            log.info("123", authenticationManager.authenticate
                    (new UsernamePasswordAuthenticationToken(username, requestDto.getPassword())));
            authenticationManager.authenticate
                    (new UsernamePasswordAuthenticationToken(username, requestDto.getPassword()));
            log.info("Authenticate :{}", requestDto.getPassword());
            User user = userService.findByUsername(username);
            log.info("Find user by username :{}", user);
            if (user == null) {
                throw new UsernameNotFoundException("User with username: " + username + " not found");
            }

            String token = jwtTokenProvider.createToken(username, user.getRoles(), validityInMilliseconds);
            String refreshToken = jwtTokenProvider.createToken(username, user.getRoles(), refreshValidityInMilliseconds);
            log.info("Create token :{}", token);
            Map<Object, Object> response = new HashMap<>();
            log.info("Put data in token");
            response.put("username", username);
            response.put("token", token);
            response.put("refreshToken", refreshToken);
            return ResponseEntity.ok(response);
        }
        catch (AuthenticationException e) {
            throw new BadCredentialsException("Invalid username or password");
        }
    }
    @PostMapping("/refreshtoken/{id}")
    public ResponseEntity<?> refreshToken(@PathVariable(name = "id") Long id) {
        try {
            User user = userService.findUserById(id);
            String token = jwtTokenProvider.createToken(user.getUsername(), user.getRoles(), validityInMilliseconds);
            String refreshToken = jwtTokenProvider.createToken(user.getUsername(), user.getRoles(), refreshValidityInMilliseconds);
            Map<Object, Object> response = new HashMap<>();
            response.put("username", user.getUsername());
            response.put("token", token);
            response.put("refreshToken", refreshToken);
            return ResponseEntity.ok(response);
        } catch (AuthenticationException e) {
            throw new BadCredentialsException("Invalid username or password");
        }
    }
}
