package com.example.demo.controller;

import com.example.demo.dto.UserRequestDto;
import com.example.demo.entity.User;
import com.example.demo.security.JwtUserDetailsService;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1/users")
public class UserController {
    private final UserService userService;
    private final JwtUserDetailsService userDetailsService;

    @Autowired
    public UserController(UserService userService, JwtUserDetailsService userDetailsService) {
        this.userService = userService;
        this.userDetailsService = userDetailsService;
    }

    @GetMapping(value = "{id}")
    public ResponseEntity<UserRequestDto> getUserById(@PathVariable(name = "id") Long id){
        User user = userService.findUserById(id);
        if(user == null){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        UserRequestDto result = UserRequestDto.fromUser(user);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}