package com.example.demo.service;

import com.example.demo.entity.User;
import org.springframework.stereotype.Service;

import java.util.List;

public interface UserService {
    User addUser(User user);

    User findByUsername(String username);

    User findUserById(Long userId);

    List<User> allUsers();

    void delete(Long id);
}
